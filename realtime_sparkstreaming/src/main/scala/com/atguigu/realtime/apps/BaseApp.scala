package com.atguigu.realtime.apps

import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
 * Created by Smexy on 2022/6/25
 *
 *    每个子类独有的变量，提取出来，让子类继承时实现
 *
 *    只有抽象类，才能定义抽象属性
 */
abstract class BaseApp {

  // 抽象属性，只定义不赋值
  var appName:String
  var groupId:String
  var topic:String
  var batchDuration:Int

  // 在子类中重写
  var context:StreamingContext= null

  //不同的需求，在构造StreamingContext传入的参数(appName,batchDuration,groupId，topic)是不同的

  def runApp(code: => Unit) :Unit={
    try {
      //将②③④作为参数传入
      code
      context.start()
      context.awaitTermination()
    } catch {
      case ex: Exception =>
        ex.printStackTrace()
        throw new RuntimeException("运行出错!")
    }
  }


}
