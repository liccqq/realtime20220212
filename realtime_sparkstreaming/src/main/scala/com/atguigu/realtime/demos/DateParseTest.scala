package com.atguigu.realtime.demos

import com.atguigu.realtime.utils.DateHandleUtil

/**
 * Created by Smexy on 2022/6/25
 */
object DateParseTest {

  def main(args: Array[String]): Unit = {

    val ts:Long = 1656139502000l

    val dateTimeStr ="2022-06-25 15:15:15"

    println(DateHandleUtil.parseMillTsToDateTime(ts))
    println(DateHandleUtil.parseMillTsToDate(ts))
    println(DateHandleUtil.parseMillTsToHour(ts))
    println(DateHandleUtil.parseDateTimeToDate(dateTimeStr))
    println(DateHandleUtil.parseDateTimeToHour(dateTimeStr))

  }

}
