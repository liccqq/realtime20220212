package com.atguigu.realtime.demos

import java.util

import com.alibaba.fastjson.{JSON, JSONObject}
import com.google.gson.Gson

/**
 * Created by Smexy on 2022/6/25
 */
object LogParseDemo {

  def main(args: Array[String]): Unit = {

    val str =
      """
        |{
        |  "common": {
        |    "ar": "530000",
        |    "ba": "Xiaomi",
        |    "ch": "oppo",
        |    "is_new": "1",
        |    "md": "Xiaomi Mix2 ",
        |    "mid": "mid_781",
        |    "os": "Android 11.0",
        |    "uid": "290",
        |    "vc": "v2.1.111"
        |  },
        |  "displays": [
        |    {
        |      "display_type": "activity",
        |      "item": "2",
        |      "item_type": "activity_id",
        |      "order": 1,
        |      "pos_id": 5
        |    },
        |    {
        |      "display_type": "activity",
        |      "item": "1",
        |      "item_type": "activity_id",
        |      "order": 2,
        |      "pos_id": 5
        |    },
        |    {
        |      "display_type": "query",
        |      "item": "311",
        |      "item_type": "sku_id",
        |      "order": 3,
        |      "pos_id": 3
        |    },
        |    {
        |      "display_type": "query",
        |      "item": "810",
        |      "item_type": "sku_id",
        |      "order": 4,
        |      "pos_id": 2
        |    },
        |    {
        |      "display_type": "promotion",
        |      "item": "949",
        |      "item_type": "sku_id",
        |      "order": 5,
        |      "pos_id": 4
        |    },
        |    {
        |      "display_type": "promotion",
        |      "item": "287",
        |      "item_type": "sku_id",
        |      "order": 6,
        |      "pos_id": 1
        |    },
        |    {
        |      "display_type": "promotion",
        |      "item": "761",
        |      "item_type": "sku_id",
        |      "order": 7,
        |      "pos_id": 1
        |    },
        |    {
        |      "display_type": "query",
        |      "item": "284",
        |      "item_type": "sku_id",
        |      "order": 8,
        |      "pos_id": 1
        |    },
        |    {
        |      "display_type": "query",
        |      "item": "198",
        |      "item_type": "sku_id",
        |      "order": 9,
        |      "pos_id": 3
        |    },
        |    {
        |      "display_type": "recommend",
        |      "item": "462",
        |      "item_type": "sku_id",
        |      "order": 10,
        |      "pos_id": 1
        |    },
        |    {
        |      "display_type": "query",
        |      "item": "672",
        |      "item_type": "sku_id",
        |      "order": 11,
        |      "pos_id": 5
        |    }
        |  ],
        |  "page": {
        |    "during_time": 3817,
        |    "page_id": "home"
        |  },
        |  "ts": 1656122032000
        |}
        |
        |
        |""".stripMargin


    val str2=
      """
        |{
        |  "common": {
        |    "ar": "310000",
        |    "ba": "iPhone",
        |    "ch": "Appstore",
        |    "is_new": "0",
        |    "md": "iPhone 8",
        |    "mid": "mid_152",
        |    "os": "iOS 13.3.1",
        |    "uid": "890",
        |    "vc": "v2.1.134"
        |  },
        |  "start": {
        |    "entry": "notice",
        |    "loading_time": 12173,
        |    "open_ad_id": 10,
        |    "open_ad_ms": 5535,
        |    "open_ad_skip_ms": 0
        |  },
        |  "ts": 1656122034000
        |}
        |
        |""".stripMargin

    // {} 转换后的map
    val map: JSONObject = JSON.parseObject(str2)

    val commonStr: String = map.getString("common")

    // 只保留了common部分
    val resultMap: JSONObject = JSON.parseObject(commonStr)

    resultMap.put("ts",map.getLong("ts"))

    println(resultMap)

    val startMap: util.Map[String, AnyRef] = JSON.parseObject(map.getString("start")).getInnerMap

    val result: util.Map[String, AnyRef] = resultMap.getInnerMap

    //两个Map类型进行putAll
    result.putAll(startMap)

    val gson = new Gson()

    println(gson.toJson(result))

  }

}
