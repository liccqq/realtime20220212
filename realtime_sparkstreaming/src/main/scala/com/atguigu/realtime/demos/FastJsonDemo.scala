package com.atguigu.realtime.demos

import com.alibaba.fastjson.JSON
import com.google.gson.Gson

/**
 * Created by Smexy on 2022/6/30
 */
object FastJsonDemo{

  def main(args: Array[String]): Unit = {

    val cat: Cat = Cat("小花猫")

    /*
    Error:(14, 18) ambiguous reference to overloaded definition,
both method toJSONString in class JSON of type (x$1: Any, x$2: com.alibaba.fastjson.serializer.SerializerFeature*)String
and  method toJSONString in class JSON of type (x$1: Any)String
match argument types (com.atguigu.realtime.demos.Cat) and expected result type Any
    println(JSON.toJSONString(cat))
     */
    //println(JSON.toJSONString(cat))

    println(new Gson().toJson(cat))

  }

}

case class Cat(name:String)
