package com.atguigu.realtime.utils

import java.time.{Instant, LocalDateTime, ZoneId}
import java.time.format.DateTimeFormatter

/**
 * Created by Smexy on 2022/6/25
 *
 *  java.time.XXX。静态方法构造
 *    日期格式:  DateTimeFormatter
 *    日期时间：  LocalDateTime
 *    日期对象:   LocalDate
 */
object DateHandleUtil {

  val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
  val dateMinuteTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
  val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
  val hourFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("HH")

  //将ts转为日期时间
  def parseMillTsToDateMiniteTime(mill:Long):String={

    val localDateTime: LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(mill), ZoneId.of("Asia/Shanghai"))

    //将时间转换为指定的格式
    localDateTime.format(dateMinuteTimeFormatter)

  }

  //将ts转为日期时间
  def parseMillTsToDateTime(mill:Long):String={

    val localDateTime: LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(mill), ZoneId.of("Asia/Shanghai"))

    //将时间转换为指定的格式
    localDateTime.format(dateTimeFormatter)

  }

  //将ts转为日期
  def parseMillTsToDate(mill:Long):String={

    val localDateTime: LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(mill), ZoneId.of("Asia/Shanghai"))

    //将时间转换为指定的格式
    localDateTime.format(dateFormatter)

  }

  //将ts转为小时
  def parseMillTsToHour(mill:Long):String={

    val localDateTime: LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(mill), ZoneId.of("Asia/Shanghai"))

    //将时间转换为指定的格式
    localDateTime.format(hourFormatter)

  }

  //将日期时间，获取日期部分
  def parseDateTimeToDate(dateTime:String):String={

    LocalDateTime.parse(dateTime,dateTimeFormatter).format(dateFormatter)

  }

  //将日期时间，获取小时部分
  def parseDateTimeToHour(dateTime:String):String={

    LocalDateTime.parse(dateTime,dateTimeFormatter).format(hourFormatter)

  }

}
