package com.atguigu.common.utils;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.common.beans.Employee;
import com.google.gson.Gson;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *      Fastjson使用静态方法。
 *      父类: JSON
 *              str 转 java对象 :   JSON.parseObject()
 *                                  JSON.parseArray()
 *
 *               java 转 str:       JSON.toJSONString(对象)
 *
 *      {}：   fastjson对应  JSONObject。 本质是Map
 *      []:    fastjson对应  JSONArray。  本质是List
 *
 *      ---------------------------------
 *      Gson都是实例方法。
 *              new Gson()
 *
 *
 */
public class FastJsonTest {

    // java对象转字符串
    @Test
    public void test1(){

        Employee jack1 = new Employee("jack1", 20);
        Employee jack2 = new Employee("jack2", 20);

        ArrayList<Employee> employees = new ArrayList<>();

        employees.add(jack1);
        employees.add(jack2);

        //[{"age":20,"name":"jack1"},{"age":20,"name":"jack2"}]
        String str1 = JSON.toJSONString(employees);

        //{"age":20,"name":"jack1"}
        String str2 = JSON.toJSONString(jack1);

        System.out.println(str1);
        System.out.println(str2);
    }

    // 字符串还原为 通用的对象
    @Test
    public void test2(){

        String str1 ="[{\"age\":20,\"name\":\"jack1\"},{\"age\":20,\"name\":\"jack2\"}]";

        String str2 = "{\"age\":20,\"name\":\"jack1\"}";

        //Map
        JSONObject jsonObject = JSON.parseObject(str2);

        System.out.println(jsonObject.getString("name"));

        // List
        JSONArray jsonArray = JSON.parseArray(str1);

        System.out.println(jsonArray.get(1));
    }

    // 字符串还原为 指定Bean
    @Test
    public void test3(){

        String str1 ="[{\"age\":20,\"name\":\"jack1\"},{\"age\":20,\"name\":\"jack2\"}]";

        String str2 = "{\"age\":20,\"name\":\"jack1\"}";

        Employee employee = JSON.parseObject(str2, Employee.class);

        System.out.println(employee);


        // List
        List<Employee> employees = JSON.parseArray(str1, Employee.class);

        System.out.println(employees.get(1));
    }

    // 把对象转为字符串
    @Test
    public void test4(){

        Employee jack1 = new Employee("jack1", 20);
        Employee jack2 = new Employee("jack2", 20);

        ArrayList<Employee> employees = new ArrayList<>();

        employees.add(jack1);
        employees.add(jack2);

        Gson gson = new Gson();

        System.out.println(gson.toJson(jack1));
        System.out.println(gson.toJson(employees));
    }


}