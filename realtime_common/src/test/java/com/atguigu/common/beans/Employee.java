package com.atguigu.common.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Smexy on 2022/6/25
 *
 *      标准的JavaBean:
 *              ①提供无参构造器
 *              ②为私有的属性提供 公共的getter,setter
 */
@Data //为所有的私有属性提供getter,setter，提供toString()
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

    private String name;
    private Integer age;
}
