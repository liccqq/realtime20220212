package com.atguigu.common.constants;

/**
 * Created by Smexy on 2022/6/25
 *      无法被实例化
 */
public interface TopicConstant {

    //log数据，解析前，统一存放的主题
    String ORIGINAL_LOG = "base_log";

    // log数据，解析后的5种行为
    String STARTUP_LOG = "REALTIME_STARTUP_LOG";
    String ERROR_LOG = "REALTIME_ERROR_LOG";
    String DISPLAY_LOG = "REALTIME_DISPLAY_LOG";
    String PAGE_LOG = "REALTIME_PAGE_LOG";
    String ACTION_LOG = "REALTIME_ACTIONS_LOG";

    //DB相关的订单和详情存放的主题
    String ORDER_INFO = "REALTIME_DB_ORDER_INFO";
    String ORDER_DETAIL = "REALTIME_DB_ORDER_DETAIL";

}
