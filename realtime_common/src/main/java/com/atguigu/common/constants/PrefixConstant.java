package com.atguigu.common.constants;

public interface PrefixConstant {

    // 第一个需求： 求每个用户每一天的首次启动日志
    String dau_redis_Preffix="startlog:";

    // 第四个需求： 购物明细灵活分析使用
    String user_info_redis_preffix= "userinfo:";
    String order_info_redis_preffix= "orderinfo:";
    String order_detail_redis_preffix= "orderdetail:";

    //在项目的生成环境对流量进行压测，测试最大的延迟时间。可以设置redis中缓存存放的时间间隔
    Integer max_delay_time = 60 * 3 ;

    Integer start_log_max_delay_time = 60 * 60 * 24 ;
}