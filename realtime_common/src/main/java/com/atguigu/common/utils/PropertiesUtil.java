package com.atguigu.common.utils;

import java.util.ResourceBundle;

/**
 * Created by Smexy on 2022/6/25
 *
 *      读取config.properties 指定属性名的 属性值
 *
 *      ResourceBundle: 读取国际化的资源文件。
 *
 *         国际化资源文件：
 *              比如开发一个App,希望在选择英语时，App所有界面都显示英文。
 *                          选择中文时，app的所有界面都显示中文。
 *                          称为国际化。
 *
 *         作用： 读取一个Properties文件。
 */
public class PropertiesUtil {

    //静态方法只能访问静态变量
    // 只写  xxx.properties中的xxx
    private static ResourceBundle config = ResourceBundle.getBundle("config");

    public static String getProperty(String name){

           return config.getString(name);

    }

    public static void main(String[] args) {

        System.out.println(getProperty("kafka.broker.list"));

    }
}
