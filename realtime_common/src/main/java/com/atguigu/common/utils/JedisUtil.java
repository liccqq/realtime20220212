package com.atguigu.common.utils;

import redis.clients.jedis.Jedis;

/**
 * Created by Smexy on 2022/6/25
 */
public class JedisUtil {

    private static String host = PropertiesUtil.getProperty("redis.host");
    private static Integer port = Integer.parseInt(PropertiesUtil.getProperty("redis.port"));

    public static Jedis getJedisClient(){
        return new Jedis(host,port);
    }

}
